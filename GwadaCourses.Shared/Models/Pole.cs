﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Newtonsoft.Json;

namespace GwadaCourses.Shared.Models
{
    public class Pole
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("courses")]
        public List<Sector> Sectors { get; set; }

        public Pole(int id, string name, IEnumerable<Sector> sectors)
        {
            Id = id;
            Name = name;

            Sectors = new List<Sector>(sectors);
        }

        public Pole(int id, string name)
            : this(id, name, Enumerable.Empty<Sector>())
        { }

        public Pole()
            : this(-1, "", Enumerable.Empty<Sector>())
        { }

        public override string ToString()
        {
            return Name;
        }
    }
}