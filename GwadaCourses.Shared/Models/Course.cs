﻿using System;
using Newtonsoft.Json;

namespace GwadaCourses.Shared.Models
{
    public class Course
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("location")]
        public string Location { get; set; }

        [JsonProperty("startDate")]
        public Nullable<DateTime> StartDate { get; set; }

        [JsonProperty("endDate")]
        public Nullable<DateTime> EndDate { get; set; }


        public Course()
            : this(-1, "")
        { }

        public Course(int id, string title)
        {
            Id = id;
            Title = title;
        }

        public override string ToString()
        {
            return Title;
        }
    }
}
