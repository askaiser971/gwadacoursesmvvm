﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Newtonsoft.Json;

namespace GwadaCourses.Shared.Models
{
    public class Sector
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("courses")]
        public List<Course> Courses { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }


        public Sector(int id, string name, IEnumerable<Course> courses)
        {
            Id = id;
            Name = name;

            Courses = new List<Course>(courses);
        }

        public Sector(int id, string name)
            : this(id, name, Enumerable.Empty<Course>())
        { }

        public Sector()
            : this(-1, "", Enumerable.Empty<Course>())
        { }

        public override string ToString()
        {
            return Name;
        }
    }
}
