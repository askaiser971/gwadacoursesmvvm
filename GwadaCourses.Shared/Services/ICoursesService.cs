﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GwadaCourses.Shared.Models;

namespace GwadaCourses.Shared.Services
{
    public interface ICoursesService
    {
        Task<IEnumerable<Pole>> GetPoles();
    }
}