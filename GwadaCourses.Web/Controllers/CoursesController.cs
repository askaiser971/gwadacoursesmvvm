﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using GwadaCourses.Shared.Models;
using GwadaCourses.Web.Database;

namespace GwadaCourses.Web.Controllers
{
    public class CoursesController : ApiController
    {
        public IEnumerable<Shared.Models.Pole> Get()
        {
            using (var db = new CoursesContext())
            {
                var poles = db.Poles.Select(p => new Shared.Models.Pole
                {
                    Id = p.IdPole,
                    Name = p.Description
                }).ToList();

                var sectors = db.Secteurs.Select(s => new Sector
                {
                    Id = s.IdSecteur,
                    Name = s.Description
                }).ToList();

                foreach (var formation in db.Formations)
                {
                    if (formation.IdPole == null)
                        continue;

                    var sector = sectors.FirstOrDefault(s => s.Id == formation.IdSecteur);
                    if (sector == null)
                        continue;

                    var pole = poles.FirstOrDefault(p => p.Id == formation.IdPole.Value);
                    if (pole == null)
                        continue;

                    if (!pole.Sectors.Contains(sector))
                        pole.Sectors.Add(sector);

                    if (sector.Courses.All(c => c.Id != formation.IdFormation))
                    {
                        sector.Courses.Add(new Course
                        {
                            Id = formation.IdFormation,
                            Title = formation.Intitule,
                            Location = formation.Lieu,
                            StartDate = formation.DateDebutFormation,
                            EndDate = formation.dateFinFormation
                        });
                    }
                }

                return poles;
            }
        }

        public string Get(int id)
        {
            return null;
        }

        public void Post([FromBody]string value)
        { }

        public void Put(int id, [FromBody]string value)
        { }

        public void Delete(int id)
        { }
    }
}