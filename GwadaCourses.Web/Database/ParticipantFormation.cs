//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GwadaCourses.Web.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class ParticipantFormation
    {
        public int IdParticipant { get; set; }
        public int IdFormation { get; set; }
        public string Etat { get; set; }
        public string Raison { get; set; }
        public Nullable<System.DateTime> DateEntree { get; set; }
    }
}
