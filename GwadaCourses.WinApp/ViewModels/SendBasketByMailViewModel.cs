﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GwadaCourses.WinApp.Core.Navigation;

namespace GwadaCourses.WinApp.ViewModels
{
    public class SendBasketByMailViewModel : ViewModelBase
    {
        const string EmailRegex =
        @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
        @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";

        private readonly INavigator _navigator;
        private readonly BasketViewModel _basket;
        private string _recipient;
        private bool _isBusy;
        private bool _mailSent;
        private string _sendMailStatus;

        /// <summary>
        /// ViewModel permettant de manipuler le panier
        /// </summary>
        public BasketViewModel Basket { get { return _basket; } }

        /// <summary>
        /// Destinataire du mail panier
        /// </summary>
        public string Recipient
        {
            get { return _recipient; }
            set
            {
                Set(() => Recipient, ref _recipient, value);
                SendMailCommand.RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        /// Permet de griser le bouton "Envoyer" pendant l'envoi du mail
        /// </summary>
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                Set(() => IsBusy, ref _isBusy, value);
                SendMailCommand.RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        /// Permet d'afficher la popup "mail envoyé" après envoi du mail
        /// </summary>
        public bool MailSent
        {
            get { return _mailSent; }
            set { Set(() => MailSent, ref _mailSent, value); }
        }

        /// <summary>
        /// Texte permettant de savoir où en est l'envoi de mail
        /// </summary>
        public string SendMailStatus
        {
            get { return _sendMailStatus; }
            set { Set(() => SendMailStatus, ref _sendMailStatus, value); }
        }

        /// <summary>
        /// Commande permettant d'envoyer le panier par mail
        /// </summary>
        public RelayCommand SendMailCommand { get; private set; }

        /// <summary>
        /// Commande permettant de revenir à l'accueil
        /// </summary>
        public RelayCommand GoToHomeCommand { get; private set; }

        /// <summary>
        /// Constructeur de la classe SendBasketByMailViewModel
        /// </summary>
        public SendBasketByMailViewModel(
            INavigator navigator,
            BasketViewModel basket)
        {
            _navigator = navigator;
            _basket = basket;

            SendMailStatus = "";

            SendMailCommand = new RelayCommand(
                SendMail,
                () => !IsBusy && Regex.IsMatch(_recipient, EmailRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250))
            );

            GoToHomeCommand = new RelayCommand(() => _navigator.GoToRoot<HomeViewModel>());
        }

        /// <summary>
        /// Commande permettant d'envoyer le panier par mail.
        /// TODO : implémenter l'envoi réel ou du moins utiliser une interface
        /// </summary>
        public async void SendMail()
        {
            IsBusy = true;
            SendMailStatus = "En cours d'envoi...";
            await Task.Delay(3000);
            
            MailSent = true;
            SendMailStatus = String.Format("Mail envoyé à l'adresse {0}", Recipient);
            await Task.Delay(3000);

            _navigator.GoToRoot<HomeViewModel>();
        }
    }
}
