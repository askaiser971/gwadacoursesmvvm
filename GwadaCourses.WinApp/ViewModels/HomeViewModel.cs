﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GwadaCourses.Shared.Models;
using GwadaCourses.Shared.Services;
using GwadaCourses.WinApp.Core.Navigation;
using GwadaCourses.WinApp.Views;

namespace GwadaCourses.WinApp.ViewModels
{
    /// <summary>
    /// ViewModel associé à l'ensemble de la page <see cref="HomeView">HomeView</see>
    /// </summary>
    public class HomeViewModel : ViewModelBase
    {
        private readonly INavigator _navigator;
        private readonly ICoursesService _service;
        private readonly Func<Pole, PoleViewModel> _poleViewModelFactory;

        /// <summary>
        /// Liste des pôles version ViewModel
        /// </summary>
        public ObservableCollection<PoleViewModel> Poles { get; private set; }

        /// <summary>
        /// Permet d'afficher un spinner lors de longues opérations
        /// </summary>
        private bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set { Set(() => IsLoading, ref _isLoading, value); }
        }

        /// <summary>
        /// Commande permettant de naviger jusqu'à un secteur (et de voir les autres secteurs du pôle parent)
        /// </summary>
        public RelayCommand<SectorViewModel> ShowPoleSectorCommand { get; private set; }

        /// <summary>
        /// Constructeur de la classe HomeViewModel
        /// </summary>
        /// <param name="navigator">Utilitaire de navigation entre page par ViewModel</param>
        /// <param name="service">Web Service de récupération des données modèles</param>
        /// <param name="poleViewModelFactory">Factory permettant d'instancier des PoleViewModel à partir d'un modèle Pole</param>
        public HomeViewModel(
            INavigator navigator,
            ICoursesService service,
            Func<Pole, PoleViewModel> poleViewModelFactory)
        {
            _navigator = navigator;
            _service = service;
            _poleViewModelFactory = poleViewModelFactory;

            Poles = new ObservableCollection<PoleViewModel>();
            GetPoles();

            ShowPoleSectorCommand = new RelayCommand<SectorViewModel>(ShowPoleSector);
        }

        /// <summary>
        /// Récupération des pôles depuis la source de données
        /// </summary>
        public async void GetPoles()
        {
            IsLoading = true;

            Poles.Clear();

            foreach (var pole in await _service.GetPoles())
            {
                Poles.Add(_poleViewModelFactory(pole));
            }

            IsLoading = false;
        }

        /// <summary>
        /// Action permettant de visualiser un secteur de pôle.
        /// Utilisé lorsque l'on clique sur un secteur dans le Hub.
        /// </summary>
        public void ShowPoleSector(SectorViewModel sector)
        {
            var pole = Poles.FirstOrDefault(p => p.Sectors.Contains(sector));
            if (pole == null)
                return;

            _navigator.Push(pole, vm =>
            {
                vm.SelectedSector = sector;
                vm.SelectedCourse = sector.Courses.FirstOrDefault();
            });
        }
    }
}
