﻿using System;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GwadaCourses.Shared.Models;
using GwadaCourses.WinApp.Core.Navigation;
using GwadaCourses.WinApp.Views;

namespace GwadaCourses.WinApp.ViewModels
{
    /// <summary>
    /// ViewModel associé à un HubSection de la page <see cref="HomeView">HomeView</see>
    /// </summary>
    public class PoleViewModel : ViewModelBase
    {
        private readonly Pole _pole;
        private readonly INavigator _navigator;
        private readonly BasketViewModel _basket;
        private readonly Func<Sector, SectorViewModel> _sectorViewModelFactory;

        private SectorViewModel _selectedSector;
        private CourseViewModel _selectedCourse;

        /// <summary>
        /// ViewModel permettant de manipuler le panier
        /// </summary>
        public BasketViewModel Basket { get { return _basket; } }

        /// <summary>
        /// Nom du pôle
        /// </summary>
        public string Name { get { return _pole.Name; } }

        /// <summary>
        /// Liste des secteurs en ViewModels
        /// </summary>
        public ObservableCollection<SectorViewModel> Sectors { get; private set; }

        /// <summary>
        /// Secteur en cours d'affichage
        /// </summary>
        public SectorViewModel SelectedSector
        {
            get { return _selectedSector; }
            set { Set(() => SelectedSector, ref _selectedSector, value); }
        }

        /// <summary>
        /// Formation en cours d'affichage. Appartient au secteur SelectedSector
        /// </summary>
        public CourseViewModel SelectedCourse
        {
            get { return _selectedCourse; }
            set { Set(() => SelectedCourse, ref _selectedCourse, value); }
        }

        /// <summary>
        /// Command permettant de revenir à la page d'accueil
        /// </summary>
        public RelayCommand GoToHomeCommand { get; private set; }

        /// <summary>
        /// Commande permettant d'ajouter la formation en cours d'affichage au panier
        /// </summary>
        public RelayCommand<CourseViewModel> AddCourseToBasketCommand { get; private set; }

        /// <summary>
        /// Commande permettant de supprimer la formation en cours d'affichage du panier
        /// </summary>
        public RelayCommand<CourseViewModel> RemoveCourseFromBasketCommand { get; private set; }

        /// <summary>
        /// Commande permettant de supprimer la formation en cours d'affichage au panier
        /// </summary>
        public RelayCommand ShowBasketCommand { get; private set; }

        /// <summary>
        /// Constructeur de la classe PoleViewModel
        /// </summary>
        /// <param name="pole">Model de pôle</param>
        /// <param name="navigator">Utilitaire de navigation entre page par ViewModel</param>
        /// <param name="basket">ViewModel qui permet de manipuler le panier</param>
        /// <param name="sectorViewModelFactory">Factory permettant de créer les ViewModels des secteurs</param>
        public PoleViewModel(
            Pole pole,
            INavigator navigator,
            BasketViewModel basket,
            Func<Sector, SectorViewModel> sectorViewModelFactory)
        {
            _pole = pole;
            _navigator = navigator;
            _basket = basket;
            _sectorViewModelFactory = sectorViewModelFactory;

            // Récupération des secteurs en tant que ViewModels
            Sectors = new ObservableCollection<SectorViewModel>();
            GetSectors(pole);

            GoToHomeCommand = new RelayCommand(() => _navigator.GoToRoot<HomeViewModel>());
            AddCourseToBasketCommand = new RelayCommand<CourseViewModel>(AddCourseToBasket);
            RemoveCourseFromBasketCommand = new RelayCommand<CourseViewModel>(RemoveCourseFromBasket);
            ShowBasketCommand = new RelayCommand(() => _navigator.Push(_basket));
        }

        public void GetSectors(Pole pole)
        {
            Sectors.Clear();

            foreach (var sector in pole.Sectors)
            {
                Sectors.Add(_sectorViewModelFactory(sector));
            }
        }

        private void AddCourseToBasket(CourseViewModel course)
        {
            _basket.AddCourseToBasket(course);
        }

        private void RemoveCourseFromBasket(CourseViewModel course)
        {
            _basket.RemoveCourseFromBasket(course);
        }
    }
}
