﻿using System;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;
using GwadaCourses.Shared.Models;

namespace GwadaCourses.WinApp.ViewModels
{
    public class SectorViewModel : ViewModelBase
    {
        private readonly Sector _sector;
        private readonly Func<Course, CourseViewModel> _courseViewModelFactory;

        public ObservableCollection<CourseViewModel> Courses { get; set; }

        public int Id { get { return _sector.Id; } }
        public string Name { get { return _sector.Name; } }
        public string Description { get { return _sector.Description; } }


        public SectorViewModel(
            Sector sector,
            Func<Course, CourseViewModel> courseViewModelFactory)
        {
            _sector = sector;
            _courseViewModelFactory = courseViewModelFactory;

            Courses = new ObservableCollection<CourseViewModel>();
            GetCourses();
        }

        public void GetCourses()
        {
            Courses.Clear();

            foreach (var course in _sector.Courses)
            {
                Courses.Add(_courseViewModelFactory(course));
            }
        }
    }
}
