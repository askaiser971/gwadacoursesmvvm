﻿using System.Collections.ObjectModel;
using System.Linq;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GwadaCourses.WinApp.Core.Navigation;

namespace GwadaCourses.WinApp.ViewModels
{
    public class BasketViewModel : ViewModelBase
    {
        private readonly INavigator _navigator;
        private CourseViewModel _selectedCourse;

        /// <summary>
        /// Liste des formations dans le panier
        /// </summary>
        public ObservableCollection<CourseViewModel> Courses { get; private set; }

        /// <summary>
        /// Formation en cours d'affichage. Appartient à la liste Courses
        /// </summary>
        public CourseViewModel SelectedCourse
        {
            get { return _selectedCourse; }
            set { Set(() => SelectedCourse, ref _selectedCourse, value); }
        }

        /// <summary>
        /// Flag permettant de savoir si le panier est rempli
        /// </summary>
        public bool HasCourses
        {
            get { return Courses.Any(); }
        }

        /// <summary>
        /// Commande permettant de retourner à l'accueil
        /// </summary>
        public RelayCommand GoToHomeCommand { get; private set; }

        /// <summary>
        /// Commande permettant de vider le panier
        /// </summary>
        public RelayCommand ClearBasketCommand { get; private set; }

        /// <summary>
        /// Commande permettant de supprimer la formation en cours d'affichage du panier
        /// </summary>
        public RelayCommand<CourseViewModel> RemoveCourseFromBasketCommand { get; private set; }

        /// <summary>
        /// Commande permettant de supprimer la formation en cours d'affichage du panier
        /// </summary>
        public RelayCommand SendBasketByMailCommand { get; private set; }


        /// <summary>
        /// Constructeur de la classe BasketViewModel
        /// </summary>
        public BasketViewModel(INavigator navigator)
        {
            _navigator = navigator;

            Courses = new ObservableCollection<CourseViewModel>();

            GoToHomeCommand = new RelayCommand(() => _navigator.GoToRoot<HomeViewModel>());
            ClearBasketCommand = new RelayCommand(ClearBasket);
            RemoveCourseFromBasketCommand = new RelayCommand<CourseViewModel>(RemoveCourseFromBasket);
            SendBasketByMailCommand = new RelayCommand(SendBasketByMail);
        }

        /// <summary>
        /// Supprimer une formation du panier et choisi la formation voisine
        /// comme étant la formation en cours de visualisation dans le panier
        /// </summary>
        public void RemoveCourseFromBasket(CourseViewModel course)
        {
            if (Courses.Contains(course) == false)
                return;

            int courseToRemoveIndex = Courses.IndexOf(course);
            CourseViewModel nextOrPreviousCourse = null;

            if (courseToRemoveIndex > 0)
                nextOrPreviousCourse = Courses[courseToRemoveIndex - 1];
            else if (Courses.Count - 1 > courseToRemoveIndex)
                nextOrPreviousCourse = Courses[courseToRemoveIndex + 1];

            if (!Courses.Remove(course))
                return;

            course.IsInBasket = false;
            SelectedCourse = nextOrPreviousCourse;
        }

        /// <summary>
        /// Ajoute une formation dans le panier et réalise le focus dessus avec SelectedCourse
        /// </summary>
        /// <param name="course"></param>
        public void AddCourseToBasket(CourseViewModel course)
        {
            if (Courses.Contains(course) == false)
            {
                Courses.Add(course);
                SelectedCourse = course;
                course.IsInBasket = true;
            }
        }

        /// <summary>
        /// Vide le panier
        /// </summary>
        private void ClearBasket()
        {
            SelectedCourse = null;
            Courses.Clear();
        }

        /// <summary>
        /// Bascule l'utilisateur vers la vue d'envoi du panier par mail
        /// </summary>
        private void SendBasketByMail()
        {
            _navigator.Push<SendBasketByMailViewModel>(vm =>
            {
                vm.Recipient = "";
            });
        }
    }
}
