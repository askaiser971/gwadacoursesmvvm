﻿using GalaSoft.MvvmLight;
using GwadaCourses.Shared.Models;

namespace GwadaCourses.WinApp.ViewModels
{
    public class CourseViewModel : ViewModelBase
    {
        private bool _isInBasket;
        public bool IsInBasket
        {
            get { return _isInBasket; }
            set { Set(() => IsInBasket, ref _isInBasket, value); }
        }

        public string Title { get; set; }


        public CourseViewModel(Course course)
        {
            IsInBasket = false;
            Title = course.Title;
        }
    }
}
