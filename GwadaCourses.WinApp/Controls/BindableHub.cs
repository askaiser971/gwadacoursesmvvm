﻿using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace GwadaCourses.WinApp.Controls
{
    public class BindableHub : Hub
    {
        /**
         * Déclaration de la zone SectionHeaderTemplate
         */
        public DataTemplate SectionHeaderTemplate
        {
            get { return (DataTemplate)GetValue(SectionHeaderTemplateProperty); }
            set { SetValue(SectionHeaderTemplateProperty, value); }
        }

        public static readonly DependencyProperty SectionHeaderTemplateProperty = DependencyProperty.Register(
            "SectionHeaderTemplate",
            typeof(DataTemplate),
            typeof(BindableHub),
            new PropertyMetadata(null, SectionHeaderTemplatePropertyChanged)
        );

        private static void SectionHeaderTemplatePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var hub = sender as BindableHub;
            if (hub == null)
                return;

            var template = e.NewValue as DataTemplate;
            if (template == null)
                return;

            foreach (var section in hub.Sections)
            {
                section.HeaderTemplate = template;
            }
        }

        /**
         * Déclaration de la zone SectionContentTemplate
         */
        public DataTemplate SectionContentTemplate
        {
            get { return (DataTemplate)GetValue(SectionContentTemplateProperty); }
            set { SetValue(SectionContentTemplateProperty, value); }
        }

        public static readonly DependencyProperty SectionContentTemplateProperty = DependencyProperty.Register(
            "SectionContentTemplate",
            typeof(DataTemplate),
            typeof(BindableHub),
            new PropertyMetadata(null, SectionContentTemplatePropertyChanged)
        );

        private static void SectionContentTemplatePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var hub = sender as BindableHub;
            if (hub == null)
                return;

            var template = e.NewValue as DataTemplate;
            if (template == null)
                return;

            foreach (var section in hub.Sections)
            {
                section.ContentTemplate = template;
            }
        }


        /**
         * Déclaration de la zone ItemsSource
         */
        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register(
            "ItemsSource",
            typeof(IEnumerable),
            typeof(BindableHub),
            new PropertyMetadata(null, ItemsSourcePropertyChanged)
        );

        private static void ItemsSourcePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var control = sender as BindableHub;
            if (control != null)
                control.ItemsSourceChanged((IEnumerable)e.OldValue, (IEnumerable)e.NewValue);
        }

        private void ItemsSourceChanged(IEnumerable oldValues, IEnumerable newValues)
        {
            // Remove handler for oldValues.CollectionChanged
            var oldCollection = oldValues as INotifyCollectionChanged;
            if (oldCollection != null)
            {
                Debug.WriteLine("ItemsSource notifyCollectionChanged detached");
                oldCollection.CollectionChanged -= ItemsSourceCollectionChanged;
            }

            // Add handler for newValues.CollectionChanged (if possible)
            var newCollection = newValues as INotifyCollectionChanged;
            if (newCollection != null)
            {
                Debug.WriteLine("ItemsSource notifyCollectionChanged attached");
                newCollection.CollectionChanged += ItemsSourceCollectionChanged;
                foreach (var newValue in newValues)
                {
                    CreateSection(newValue);
                }
            }
        }

        /// <summary>
        /// ItemsSources à changé, on ajoute ou supprime des HubSection en conséquence
        /// </summary>
        private void ItemsSourceCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    CreateSections(e.NewItems);
                    break;
                case NotifyCollectionChangedAction.Remove:
                    RemoveSections(e.OldItems);
                    break;
                case NotifyCollectionChangedAction.Replace:
                    RemoveSections(e.OldItems);
                    CreateSections(e.NewItems);
                    break;
                case NotifyCollectionChangedAction.Reset:
                    this.Sections.Clear();
                    break;
            }
        }

        private void CreateSections(IEnumerable dataContexts)
        {
            if (dataContexts == null)
                return;

            foreach (var dataContext in dataContexts)
            {
                CreateSection(dataContext);
            }
        }

        private void RemoveSections(IEnumerable dataContexts)
        {
            if (dataContexts == null)
                return;

            foreach (var dataContext in dataContexts)
            {
                RemoveSection(dataContext);
            }
        }

        /// <summary>
        /// Ajoute une section si son DataContext à été ajouté à la liste ItemsSource
        /// </summary>
        private void CreateSection(object dataContext)
        {
            var section = new HubSection
            {
                DataContext = dataContext,
                ContentTemplate = this.SectionContentTemplate,
                HeaderTemplate = this.SectionHeaderTemplate
            };

            this.Sections.Add(section);
        }

        /// <summary>
        /// Supprime une section si son DataContext n'est plus dans la liste ItemsSource
        /// </summary>
        private void RemoveSection(object dataContext)
        {
            var sectionsToRemove = (
                from section in this.Sections
                where section.DataContext == dataContext
                select section
            );

            foreach (var section in sectionsToRemove)
            {
                this.Sections.Remove(section);
            }
        }
    }
}
