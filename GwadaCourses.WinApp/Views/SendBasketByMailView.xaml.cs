﻿using Windows.UI.Xaml.Navigation;

namespace GwadaCourses.WinApp.Views
{
    public sealed partial class SendBasketByMailView
    {
        public SendBasketByMailView()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            DataContext = e.Parameter;
        }
    }
}
