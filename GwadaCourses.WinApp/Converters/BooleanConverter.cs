﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml.Data;

namespace GwadaCourses.WinApp.Converters
{
    public class BooleanConverter<T> : IValueConverter
    {
        public T TrueObject { get; set; }
        
        public T FalseObject { get; set; }


        public BooleanConverter(T trueObjectValue, T falseObjectValue)
        {
            TrueObject = trueObjectValue;
            FalseObject = falseObjectValue;
        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return value is bool && ((bool)value) ? TrueObject : FalseObject;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value is T && EqualityComparer<T>.Default.Equals((T)value, TrueObject);
        }
    }
}
