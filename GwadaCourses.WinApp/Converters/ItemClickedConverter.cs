﻿using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;

namespace GwadaCourses.WinApp.Converters
{
    /// <summary>
    /// Ce converter sert à binder une commande sur l'énènement ItemClick d'un contrôle
    /// et d'envoyer le ClickedItem (dataSource bindé à l'objet cliqué) en tant que paramètre
    /// de la commande
    /// </summary>
    public class ItemClickedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var args = value as ItemClickEventArgs;

            if (args != null)
                return args.ClickedItem;

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
