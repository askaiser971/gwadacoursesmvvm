﻿using System;
using Windows.UI.Xaml.Data;
using GwadaCourses.WinApp.ViewModels;

namespace GwadaCourses.WinApp.Converters
{
    public class SectorImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var sector = value as SectorViewModel;
            if (sector == null || sector.Id <= 0)
                return null;

            return String.Format("/Assets/Sectors/{0}.png", sector.Id);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }
    }
}
