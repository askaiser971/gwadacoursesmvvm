﻿using Autofac;
using GwadaCourses.WinApp.Core.Navigation;

namespace GwadaCourses.WinApp.Core.Bootstrapping
{
    public abstract class AutofacBootstrapper
    {
        public void Run()
        {
            var builder = new ContainerBuilder();

            ConfigureContainer(builder);

            var container = builder.Build();
            var navigator = container.Resolve<INavigator>();

            RegisterViews(navigator);

            ConfigureApplication(container);
        }

        protected virtual void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule<AutofacModule>();
        }

        protected abstract void RegisterViews(INavigator navigator);

        protected abstract void ConfigureApplication(IContainer container);
    }
}
