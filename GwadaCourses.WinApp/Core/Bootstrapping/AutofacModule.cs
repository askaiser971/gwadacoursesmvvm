﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Autofac;
using GwadaCourses.WinApp.Core.Navigation;

namespace GwadaCourses.WinApp.Core.Bootstrapping
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // viewModel navivation registration
            builder.RegisterType<Navigator>()
                .As<INavigator>()
                .SingleInstance();

            // native page navigation registration
            builder.Register(context =>
                Window.Current.Content as Frame
            ).SingleInstance();
        }
    }
}
