﻿using Autofac;
using GwadaCourses.WinApp.Core.Navigation;
using GwadaCourses.WinApp.ViewModels;
using GwadaCourses.WinApp.Views;

namespace GwadaCourses.WinApp.Core.Bootstrapping
{
    public class Bootstrapper : AutofacBootstrapper
    {
        protected override void ConfigureContainer(ContainerBuilder builder)
        {
            base.ConfigureContainer(builder);
            builder.RegisterModule<GwadaCoursesModule>();
        }

        protected override void RegisterViews(INavigator navigator)
        {
            navigator.Register<HomeViewModel, HomeView>();
            navigator.Register<PoleViewModel, PoleView>();
            navigator.Register<BasketViewModel, BasketView>();
            navigator.Register<SendBasketByMailViewModel, SendBasketByMailView>();
        }

        protected override void ConfigureApplication(IContainer container)
        {
            // set main page
            var navigator = container.Resolve<INavigator>();
            navigator.Push<HomeViewModel>();
        }
    }
}
