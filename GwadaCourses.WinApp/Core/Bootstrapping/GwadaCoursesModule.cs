﻿using Autofac;
using GwadaCourses.Shared.Services;
using GwadaCourses.WinApp.Services;
using GwadaCourses.WinApp.ViewModels;

namespace GwadaCourses.WinApp.Core.Bootstrapping
{
    public class GwadaCoursesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
#if DEBUG
            builder.RegisterType<FakeCoursesService>()
                .As<ICoursesService>()
                .SingleInstance();
#else
            builder.RegisterType<CoursesWebService>()
                .As<ICoursesService>()
                .SingleInstance();
#endif

            builder.RegisterType<HomeViewModel>().SingleInstance();
            builder.RegisterType<BasketViewModel>().SingleInstance();

            builder.RegisterType<PoleViewModel>();
            builder.RegisterType<SectorViewModel>();
            builder.RegisterType<CourseViewModel>();
            builder.RegisterType<SendBasketByMailViewModel>();
        }
    }
}
