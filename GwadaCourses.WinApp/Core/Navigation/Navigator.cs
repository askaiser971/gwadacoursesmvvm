﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml.Controls;
using Autofac;
using GalaSoft.MvvmLight;

namespace GwadaCourses.WinApp.Core.Navigation
{
    class Navigator : INavigator
    {
        private readonly Lazy<Frame> _mainFrame;

        private readonly IDictionary<Type, Type> _map = new Dictionary<Type, Type>();
        private readonly IComponentContext _componentContext;

        public Navigator(IComponentContext componentContext, Lazy<Frame> mainFrame)
        {
            _componentContext = componentContext;
            _mainFrame = mainFrame;
        }

        private Frame MainFrame
        {
            get { return _mainFrame.Value; }
        }

        public void Register<TViewModel, TView>()
            where TViewModel : ViewModelBase
            where TView : Page
        {
            _map[typeof(TViewModel)] = typeof(TView);
        }

        public TViewModel GoBack<TViewModel>(Action<TViewModel> setStateAction = null)
            where TViewModel : ViewModelBase
        {
            if (MainFrame.CanGoBack)
                MainFrame.GoBack();

            var page = MainFrame.Content as Page;
            if (page == null)
                return null;

            var viewModel = page.DataContext as TViewModel;

            if (viewModel != null && setStateAction != null)
                setStateAction.Invoke(viewModel);

            return viewModel;
        }

        public TViewModel GoToRoot<TViewModel>(Action<TViewModel> setStateAction = null)
            where TViewModel : ViewModelBase
        {
            while (MainFrame.CanGoBack)
                MainFrame.GoBack();

            var page = MainFrame.Content as Page;
            if (page == null)
                return null;

            var viewModel = page.DataContext as TViewModel;

            if (viewModel != null && setStateAction != null)
                setStateAction.Invoke(viewModel);

            return viewModel;
        }

        public TViewModel Push<TViewModel>(Action<TViewModel> setStateAction = null)
            where TViewModel : ViewModelBase
        {
            var viewModel = _componentContext.Resolve<TViewModel>();

            if (setStateAction != null)
                setStateAction.Invoke(viewModel);

            var viewType = _map[typeof(TViewModel)];

            MainFrame.Navigate(viewType, viewModel);

            return viewModel;
        }

        public TViewModel Push<TViewModel>(TViewModel viewModel, Action<TViewModel> setStateAction = null)
            where TViewModel : ViewModelBase
        {
            if (setStateAction != null)
                setStateAction.Invoke(viewModel);

            var viewType = _map[typeof(TViewModel)];
            
            MainFrame.Navigate(viewType, viewModel);
            
            return viewModel;
        }
    }
}
