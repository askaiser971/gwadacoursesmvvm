﻿using System;
using Windows.UI.Xaml.Controls;
using GalaSoft.MvvmLight;

namespace GwadaCourses.WinApp.Core.Navigation
{
    public interface INavigator
    {
        void Register<TViewModel, TView>()
            where TViewModel : ViewModelBase
            where TView : Page;

        TViewModel GoBack<TViewModel>(Action<TViewModel> setStateAction = null)
            where TViewModel : ViewModelBase;

        TViewModel GoToRoot<TViewModel>(Action<TViewModel> setStateAction = null)
            where TViewModel : ViewModelBase;

        TViewModel Push<TViewModel>(Action<TViewModel> setStateAction = null)
            where TViewModel : ViewModelBase;

        TViewModel Push<TViewModel>(TViewModel viewModel, Action<TViewModel> setStateAction = null)
            where TViewModel : ViewModelBase;
    }
}