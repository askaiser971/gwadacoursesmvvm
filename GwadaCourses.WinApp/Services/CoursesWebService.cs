﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using GwadaCourses.Shared.Models;
using GwadaCourses.Shared.Services;
using Newtonsoft.Json;

namespace GwadaCourses.WinApp.Services
{
    public class CoursesWebService : ICoursesService
    {
        private const string Url = "http://localhost:53878/api/courses";

        public async Task<IEnumerable<Pole>> GetPoles()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var json = await client.GetStringAsync(Url);
            return JsonConvert.DeserializeObject<IEnumerable<Pole>>(json);
        }
    }
}
