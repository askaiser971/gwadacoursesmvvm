﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.Storage;
using GwadaCourses.Shared.Models;
using GwadaCourses.Shared.Services;
using Newtonsoft.Json;

namespace GwadaCourses.WinApp.Services
{
    public class FakeCoursesService : ICoursesService
    {
        public async Task<IEnumerable<Pole>> GetPoles()
        {
            var assets = await Package.Current.InstalledLocation.GetFolderAsync("Assets");
            var jsonFile = await assets.GetFileAsync("data.json");
            var json = await FileIO.ReadTextAsync(jsonFile);

            return JsonConvert.DeserializeObject<IEnumerable<Pole>>(json);
        }
    }
}
